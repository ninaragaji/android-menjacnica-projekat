package com.example.currencyexchange;

import android.os.AsyncTask;
import android.util.Log;

public class IzracunajValute extends AsyncTask<Object, Void, Double> {

    private static final String TAG = IzracunajValute.class.getSimpleName();

    private MainActivity activity;

    public IzracunajValute(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        activity.prikaziOcitavanje();
    }

    @Override
    protected Double doInBackground(Object... params) {
        String iz = (String) params[0];
        String u = (String) params[1];
        double vrednost = (double) params[2];
        return ValuteAPI.zameniValute(iz, u, vrednost);
    }

    @Override
    protected void onPostExecute(Double result) {
        super.onPostExecute(result);
        Log.d(TAG, "Izračunata vrednost je " + result);
        activity.prikaziRezultat(result);
        activity.resetIzracunajDugme();
    }
}

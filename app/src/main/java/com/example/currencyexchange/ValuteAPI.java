package com.example.currencyexchange;

import android.util.Log;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ValuteAPI {

    private static final String TAG = ValuteAPI.class.getSimpleName();
    private static final String host = "currency-exchange.p.rapidapi.com";
    private static final String url = "https://currency-exchange.p.rapidapi.com";
    private static final String apiKey = "e65b0bfedcmshdc24ec99a5ea049p1cdf76jsnaa1a99570ac4";

    public static String dobaviValute() {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url + "/listquotes")
                .header("X-RapidAPI-Key", apiKey)
                .header("X-RapidAPI-Host", host)
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                return response.body().string();
            } else {
                Log.e(TAG, "Error: " + response.code());
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }

        return null;
    }

    public static double zameniValute(String from, String to, double amount) {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url + "/exchange?from=" + from + "&to=" + to)
                .header("X-RapidAPI-Key", apiKey)
                .header("X-RapidAPI-Host", host)
                .build();

        try {
            Response response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                String result = response.body().string();
                double exchangeRate = Double.parseDouble(result);
                return exchangeRate * amount;
            } else {
                Log.e(TAG, "Error: " + response.code());
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }

        return 0.0;
    }
}

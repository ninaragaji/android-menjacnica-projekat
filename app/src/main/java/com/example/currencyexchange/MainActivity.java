package com.example.currencyexchange;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Spinner izValute;
    private Spinner uValutu;
    private Button izracunajDugme;
    private TextView rezultat;
    private EditText vrednost;

    private ArrayAdapter<String> adapter;

    private IzracunajValute izracunajValute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        izValute = findViewById(R.id.spinner_iz_valute);
        uValutu = findViewById(R.id.spinuer_u_valutu);
        izracunajDugme = findViewById(R.id.dugme_izracunaj);
        rezultat = findViewById(R.id.rezultat_tekst);
        vrednost = findViewById(R.id.vrednost_tekst);
        Button dugmeZameni = findViewById(R.id.dugme_zameni);
        Button dugmeIsprazni = findViewById(R.id.dugme_isprazni);

        List<String> currenciesList = new ArrayList<>();

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, currenciesList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        izValute.setAdapter(adapter);
        uValutu.setAdapter(adapter);

        DobaviSveValute dobaviSveValute = new DobaviSveValute(this, currenciesList);
        dobaviSveValute.execute();

        dugmeIsprazni.setOnClickListener(v -> {
            rezultat.setText("");
            vrednost.setText("");
        });

        dugmeZameni.setOnClickListener(v -> {
            int fromIndex = izValute.getSelectedItemPosition();
            int toIndex = uValutu.getSelectedItemPosition();

            izValute.setSelection(toIndex);
            uValutu.setSelection(fromIndex);
        });

        izracunajDugme.setOnClickListener(v -> {
            String izValuteTekst = izValute.getSelectedItem().toString();
            String uValutuTekst = uValutu.getSelectedItem().toString();
            double vrednostDouble = 0;

            String vrednostTekst = vrednost.getText().toString();
            if (!TextUtils.isEmpty(vrednostTekst)) {
                vrednostDouble = Double.parseDouble(vrednostTekst);
            }

            izracunajValute = new IzracunajValute(MainActivity.this);
            izracunajValute.execute(izValuteTekst, uValutuTekst, vrednostDouble);
        });
    }

    public void promeniVrednostPadajucihMenia() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void postaviPocetneVrednosti() {
        if (adapter != null) {
            izValute.setSelection(adapter.getPosition("EUR"));
            uValutu.setSelection(adapter.getPosition("USD"));
        }
    }

    public void prikaziSve() {
        izValute.setVisibility(View.VISIBLE);
        uValutu.setVisibility(View.VISIBLE);
        izracunajDugme.setVisibility(View.VISIBLE);
    }

    public void prikaziOcitavanje() {
        izracunajDugme.setEnabled(false);
        izracunajDugme.setText("Izračunavanje...");
    }

    public void prikaziRezultat(Double rez) {
        rezultat.setText("Izračunata vrednost je " + rez);
        rezultat.setVisibility(View.VISIBLE);
    }

    public void resetIzracunajDugme() {
        izracunajDugme.setEnabled(true);
        izracunajDugme.setText("Izračunaj");
    }
}

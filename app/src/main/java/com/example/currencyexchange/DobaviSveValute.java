package com.example.currencyexchange;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class DobaviSveValute extends AsyncTask<Void, Void, String> {

    private static final String TAG = DobaviSveValute.class.getSimpleName();

    private final List<String> valuteLista;
    private final MainActivity activity;

    public DobaviSveValute(MainActivity activity, List<String> currenciesList) {
        this.activity = activity;
        this.valuteLista = currenciesList;
    }

    @Override
    protected String doInBackground(Void... voids) {
        return ValuteAPI.dobaviValute();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result != null) {
            try {
                JSONArray currenciesArray = new JSONArray(result);
                valuteLista.clear();
                for (int i = 0; i < currenciesArray.length(); i++) {
                    String currencyCode = currenciesArray.getString(i);
                    valuteLista.add(currencyCode);
                }

                activity.promeniVrednostPadajucihMenia();
                activity.postaviPocetneVrednosti();
                activity.prikaziSve();
            } catch (JSONException e) {
                Log.e(TAG, "JSON Greska: " + e.getMessage());
            }
        }
    }
}
